CREATE TABLE book (
    id SERIAL PRIMARY KEY,
    code VARCHAR(10) UNIQUE,
    title VARCHAR(255),
    author VARCHAR(255),
    stock INTEGER,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP
);

CREATE OR REPLACE FUNCTION update_updated_at()
RETURNS TRIGGER AS $$
BEGIN
    NEW.updated_at = NOW();
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trigger_update_updated_at
BEFORE UPDATE ON book
FOR EACH ROW
EXECUTE FUNCTION update_updated_at();

INSERT INTO book (code, title, author, stock) VALUES
    ('JK-45', 'Harry Potter', 'J.K Rowling', 1),
    ('SHR-1', 'A Study in Scarlet', 'Arthur Conan Doyle', 1),
    ('TW-11', 'Twilight', 'Stephenie Meyer', 1),
    ('HOB-83', 'The Hobbit, or There and Back Again', 'J.R.R. Tolkien', 1),
    ('NRN-7', 'The Lion, the Witch and the Wardrobe', 'C.S. Lewis', 1);


CREATE TABLE members (
    id SERIAL PRIMARY KEY,
    code VARCHAR(10) UNIQUE,
    name VARCHAR(255),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP
);

CREATE TRIGGER trigger_update_updated_at
BEFORE UPDATE ON members
FOR EACH ROW
EXECUTE FUNCTION update_updated_at();

INSERT INTO members (code, name) VALUES
    ('M001', 'Angga'),
    ('M002', 'Ferry'),
    ('M003', 'Putri');
		
CREATE TABLE borrowed_book (
    id SERIAL PRIMARY KEY,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP WITH TIME ZONE,
    returned_at TIMESTAMP WITH TIME ZONE,
    member_id INT NOT NULL,
    book_id INT NOT NULL,
    FOREIGN KEY (member_id) REFERENCES members(id),
    FOREIGN KEY (book_id) REFERENCES book(id)
);

CREATE TRIGGER trigger_update_updated_at
BEFORE UPDATE ON borrowed_book
FOR EACH ROW
EXECUTE FUNCTION update_updated_at();

CREATE TABLE suspended_member (
    id SERIAL PRIMARY KEY,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP,
    member_id INT NOT NULL,
    suspended_until TIMESTAMP,
    CONSTRAINT fk_member
      FOREIGN KEY(member_id) 
	  REFERENCES members(id)
);

CREATE TRIGGER trigger_update_updated_at
BEFORE UPDATE ON suspended_member
FOR EACH ROW
EXECUTE FUNCTION update_updated_at();