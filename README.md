## README for Library and Algorithm Test Project

This project consists of two main parts: Library Management and Algorithm Test.

### Library Management

The Library Management part of the project facilitates the borrowing of books by members. It involves two main entities: Member and Book. The system enforces certain rules for book borrowing.

### Algorithm Test

The Algorithm Test section contains various algorithms for solving different problems.

#### Available Algorithms:

1. **Reverse Alphabet**: Given a string, reverses the alphabet characters while keeping the digits and symbols unchanged.
2. **Find Word with Max Length**: Given a sentence, finds the word with the maximum length. If multiple words have the same maximum length, one of them is returned.
3. **Count Word from 2 Arrays**: Counts occurrences of words from one array in another array
4. **Calculate Diagonal Difference from Matrix NxN**: Calculates the absolute difference between the sums of the diagonals of a square matrix.

### Getting Started

1. **Set up the environment**: Copy the `example.env` file to `.env` and configure the database host and other environment variables to match your local setup.
   `cp example.env .env`
2. Execute the `eigen.sql` file located in the root directory to set up the database.
3. Install the required packages by running:
    ```yarn install```
4. Start the project with: 
    ```yarn start```
5. Access the Swagger documentation at `http://localhost:3000/api` to explore and interact with the Library Management and Algorithm Test APIs.

### Running Unit Tests

- You can run unit tests for the project using the following command: `yarn test`