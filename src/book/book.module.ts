import { Module } from '@nestjs/common';
import { BookController } from './book.controller';
import { BookService } from './book.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BookEntity } from 'src/entities/book/book.entity';
import { MembersEntity } from 'src/entities/member/member.entity';
import { BorrowedBookEntity } from 'src/entities/book/borrowed-book.entity';
import { SuspendedMemberEntity } from 'src/entities/member/suspended-member.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      BookEntity,
      MembersEntity,
      BorrowedBookEntity,
      SuspendedMemberEntity
    ]),
  ],
  controllers: [BookController],
  providers: [BookService]
})
export class BookModule { }
