import { Test, TestingModule } from '@nestjs/testing';
import { BookController } from './book.controller';
import { BookService } from './book.service';
import { PaginateDto } from 'src/utils/dto/paginate-dto';
import { BorrowDto } from 'src/utils/dto/book/borrow-dto';

describe('BookController', () => {
  let controller: BookController;
  let service: BookService;

  const mockBookService = {
    findBook: jest.fn(),
    findMemberAndBookBorrowed: jest.fn(),
    borrowBook: jest.fn(),
    returnBook: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BookController],
      providers: [
        {
          provide: BookService,
          useValue: mockBookService,
        },
      ],
    }).compile();

    controller = module.get<BookController>(BookController);
    service = module.get<BookService>(BookService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('findBooks', () => {
    it('should call findBook method of the service with correct parameters', async () => {
      const paginateDto: PaginateDto = { page: 1, pageSize: 10, filter: '', sortBy: 'id', orderBy: 'ASC' };
      await controller.findBooks(paginateDto);
      expect(service.findBook).toHaveBeenCalledWith(1, 10, '', 'id', 'ASC');
    });
  });

  describe('findMemberAndBook', () => {
    it('should call findMemberAndBookBorrowed method of the service with correct parameters', async () => {
      const paginateDto: PaginateDto = { page: 1, pageSize: 10, filter: '', sortBy: 'id', orderBy: 'ASC' };
      await controller.findMemberAndBook(paginateDto);
      expect(service.findMemberAndBookBorrowed).toHaveBeenCalledWith(1, 10, '', 'id', 'ASC');
    });
  });

  describe('borrow', () => {
    it('should call borrowBook method of the service with correct parameters', async () => {
      const borrowDto: BorrowDto = { memberCode: 'M001', bookCode: 'B001' };
      await controller.borrow(borrowDto);
      expect(service.borrowBook).toHaveBeenCalledWith('M001', 'B001');
    });
  });

  describe('returnBook', () => {
    it('should call returnBook method of the service with correct parameters', async () => {
      const borrowDto: BorrowDto = { memberCode: 'M001', bookCode: 'B001' };
      await controller.returnBook(borrowDto);
      expect(service.returnBook).toHaveBeenCalledWith('M001', 'B001');
    });
  });
});
