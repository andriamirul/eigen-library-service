import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { BookService } from './book.service';
import { ApiQuery, ApiTags } from '@nestjs/swagger';
import { PaginateDto } from 'src/utils/dto/paginate-dto';
import { BorrowDto } from 'src/utils/dto/book/borrow-dto';

@ApiTags('Library')
@Controller('book')
export class BookController {
    constructor(
        private readonly bookService: BookService
    ) { }

    @Get('check-book')
    async findBooks(
        @Query() req: PaginateDto
    ) {
        return await this.bookService.findBook(req.page, req.pageSize, req.filter, req.sortBy, req.orderBy)
    }

    @Get('check-member')
    async findMemberAndBook(
        @Query() req: PaginateDto
    ) {
        return await this.bookService.findMemberAndBookBorrowed(req.page, req.pageSize, req.filter, req.sortBy, req.orderBy)
    }

    @Post('borrow')
    async borrow(
        @Body() req: BorrowDto
    ) {
        return await this.bookService.borrowBook(req.memberCode, req.bookCode)
    }

    @Post('return')
    async returnBook(
        @Body() req: BorrowDto
    ) {
        return await this.bookService.returnBook(req.memberCode, req.bookCode)
    }
}
