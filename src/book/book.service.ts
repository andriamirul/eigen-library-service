import { HttpException, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BookEntity } from 'src/entities/book/book.entity';
import { BorrowedBookEntity } from 'src/entities/book/borrowed-book.entity';
import { MembersEntity } from 'src/entities/member/member.entity';
import { SuspendedMemberEntity } from 'src/entities/member/suspended-member.entity';
import { Repository, DataSource } from 'typeorm';

@Injectable()
export class BookService {
    constructor(
        @InjectRepository(BookEntity)
        private bookRepository: Repository<BookEntity>,
        @InjectRepository(MembersEntity)
        private memberEntity: Repository<MembersEntity>,
        @InjectRepository(BorrowedBookEntity)
        private borrowedBookEntity: Repository<BorrowedBookEntity>,
        @InjectRepository(SuspendedMemberEntity)
        private suspendedMemberEntity: Repository<SuspendedMemberEntity>,
        private dataSource: DataSource

    ) { }

    async findBook(pageNumber: number, pageSize: number, filter: string, sortBy: string, orderBy: string) {
        let query = `SELECT id, code, title, author, stock FROM book WHERE stock > $1`
        const parameters = [];
        parameters.push(0);

        if (filter) {
            query += ` AND title ILIKE $${parameters.length + 1} OR code ILIKE $${parameters.length + 1} OR author ILIKE $${parameters.length + 1}`;
            parameters.push(`%${filter}%`);
        }

        if (sortBy && orderBy) {
            const order = orderBy.toUpperCase() === 'ASC' ? 'ASC' : 'DESC';
            query += ` ORDER BY $${parameters.length + 1} ${order}`;
            parameters.push(sortBy);
        } else {
            query += ` ORDER BY id DESC`;
        }

        if (!pageNumber) {
            pageNumber = 1
        }

        if (!pageSize) {
            pageSize = 10
        }

        const offset = (pageNumber - 1) * pageSize
        query += ` LIMIT $${parameters.length + 1}`
        parameters.push(pageSize);
        query += ` OFFSET $${parameters.length + 1}`;
        parameters.push(offset);

        const res = await this.bookRepository.query(query, parameters)
        return res
    }

    async borrowBook(memberCode: string, bookCode: string) {
        const queryRunner = this.dataSource.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {
            const getBook = await this.bookRepository.query(`SELECT * FROM book WHERE code = $1 AND stock > $2 AND deleted_at IS NULL`, [bookCode, 0])
            if (getBook.length == 0) {
                throw new HttpException("Book not found", HttpStatus.NOT_FOUND)
            }
            const getMember = await this.memberEntity.query(`SELECT * FROM members WHERE code = $1 AND deleted_at IS NULL`, [memberCode])
            if (getMember.length == 0) {
                throw new HttpException("Member not found", HttpStatus.NOT_FOUND)
            }

            const isMaxBorrow = await this.borrowedBookEntity.query(`SELECT * FROM borrowed_book WHERE member_id = $1 AND deleted_at IS NULL AND returned_at IS NULL`, [getMember[0].id])
            if (isMaxBorrow.length >= 2) {
                throw new HttpException("Member already reached max borrowed book", HttpStatus.TOO_MANY_REQUESTS)
            }

            const isBookBorrowed = await this.borrowedBookEntity.query(`SELECT * FROM borrowed_book WHERE book_id = $1 AND deleted_at IS NULL AND returned_at IS NULL`, [getBook[0].id])
            if (isBookBorrowed.length > 0) {
                throw new HttpException("Book is borrowed by someone else", HttpStatus.CONFLICT)
            }

            const isSuspended = await this.suspendedMemberEntity.query(`SELECT * FROM suspended_member WHERE member_id = $1 AND deleted_at IS NULL`, [getMember[0].id])
            if (isSuspended.length > 0 && isSuspended[0]?.suspended_until) {
                const suspendedUntil = new Date(isSuspended[0].suspended_until).getTime()
                if (Date.now() <= suspendedUntil) {
                    throw new HttpException("Member is supended until " + isSuspended[0].suspended_until, HttpStatus.FORBIDDEN)
                }
            }

            await queryRunner.manager.query(`INSERT INTO borrowed_book (member_id, book_id) VALUES ($1,$2)`, [getMember[0].id, getBook[0].id])
            let stock = getBook[0].stock - 1
            if (stock < 0) {
                stock = 0
            }
            await queryRunner.manager.query(`UPDATE book SET stock = $1 WHERE id = $2`, [stock, getBook[0].id])

            await queryRunner.commitTransaction();

            return null
        } catch (err) {
            await queryRunner.rollbackTransaction();
            const errorCode = err?.status ?? HttpStatus.INTERNAL_SERVER_ERROR

            throw new HttpException(err, errorCode);
        } finally {
            await queryRunner.release();
        }
    }

    async returnBook(memberCode: string, bookCode: string) {
        const queryRunner = this.dataSource.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {
            const getBook = await this.bookRepository.query(`SELECT * FROM book WHERE code = $1`, [bookCode])
            if (getBook.length == 0) {
                throw new HttpException("Book not found", HttpStatus.NOT_FOUND)
            }

            const getMember = await this.memberEntity.query(`SELECT * FROM members WHERE code = $1 AND deleted_at IS NULL`, [memberCode])
            if (getMember.length == 0) {
                throw new HttpException("Member not found", HttpStatus.NOT_FOUND)
            }

            const checkBorrow = await this.borrowedBookEntity.query(`SELECT * FROM borrowed_book WHERE member_id = $1 AND book_id = $2 AND deleted_at IS NULL AND returned_at IS NULL`, [getMember[0].id, getBook[0].id])
            if (checkBorrow.length == 0) {
                throw new HttpException("The book is not borrowed by this member", HttpStatus.NOT_FOUND)
            }

            const borrowedDate = checkBorrow[0].created_at.getTime();

            const sevenDaysInMilliseconds = 7 * 24 * 60 * 60 * 1000;
            const sevenDaysLater = borrowedDate + sevenDaysInMilliseconds;
            const currentTime = new Date().getTime();

            if (currentTime > sevenDaysLater) {
                const isSuspended = await this.suspendedMemberEntity.query(`SELECT * FROM suspended_member WHERE member_id = $1 AND deleted_at IS NULL`, [getMember[0].id])
                const threeDaysInMilliseconds = 3 * 24 * 60 * 60 * 1000; // Calculate 3 days in milliseconds
                const threeDaysLater = new Date().getTime() + threeDaysInMilliseconds;

                if (isSuspended.length > 0) {
                    await queryRunner.manager.query(
                        `UPDATE suspended_member SET suspended_until = $1 WHERE member_id = $2 AND deleted_at IS NULL`,
                        [new Date(threeDaysLater), getMember[0].id]
                    );
                } else {
                    await queryRunner.manager.query(
                        `INSERT INTO suspended_member (suspended_until,member_id) VALUES ($1, $2)`,
                        [new Date(threeDaysLater), getMember[0].id]
                    )
                }

            }

            await queryRunner.manager.query(`UPDATE borrowed_book SET returned_at = $1 WHERE id = $2`, [new Date(), checkBorrow[0].id])
            let stock = getBook[0].stock + 1
            await queryRunner.manager.query(`UPDATE book SET stock = $1 WHERE id = $2`, [stock, getBook[0].id])

            await queryRunner.commitTransaction();

            return null
        } catch (err) {
            await queryRunner.rollbackTransaction();
            const errorCode = err?.status ?? HttpStatus.INTERNAL_SERVER_ERROR

            throw new HttpException(err, errorCode);
        } finally {
            await queryRunner.release();
        }
    }

    async findMemberAndBookBorrowed(pageNumber: number, pageSize: number, filter: string, sortBy: string, orderBy: string) {
        let query = `SELECT 
        id, 
        code, 
        name, 
        (SELECT COUNT(1) FROM borrowed_book WHERE member_id = members.id AND returned_at IS NULL AND deleted_at IS NULL) AS borrowed_books 
        FROM members`
        const parameters = [];

        if (filter) {
            query += ` WHERE code ILIKE $${parameters.length + 1} OR name ILIKE $${parameters.length + 1}`;
            parameters.push(`%${filter}%`);
        }

        if (sortBy && orderBy) {
            const order = orderBy.toUpperCase() === 'ASC' ? 'ASC' : 'DESC';
            if (sortBy == "borrowed_books") {
                query += ` ORDER BY borrowed_books ${order}`;
            } else {
                query += ` ORDER BY $${parameters.length + 1} ${order}`;
                parameters.push(sortBy);
            }
        } else {
            query += ` ORDER BY id DESC`;
        }

        if (!pageNumber) {
            pageNumber = 1
        }

        if (!pageSize) {
            pageSize = 10
        }

        const offset = (pageNumber - 1) * pageSize
        query += ` LIMIT $${parameters.length + 1}`
        parameters.push(pageSize);
        query += ` OFFSET $${parameters.length + 1}`;
        parameters.push(offset);

        const res = await this.memberEntity.query(query, parameters)
        return res
    }
}
