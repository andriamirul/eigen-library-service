import { Test, TestingModule } from '@nestjs/testing';
import { HttpException, HttpStatus } from '@nestjs/common';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository, DataSource } from 'typeorm';
import { BookService } from './book.service';
import { BorrowedBookEntity } from 'src/entities/book/borrowed-book.entity';
import { MembersEntity } from 'src/entities/member/member.entity';
import { SuspendedMemberEntity } from 'src/entities/member/suspended-member.entity';
import { BookEntity } from 'src/entities/book/book.entity';

describe('BookService', () => {
  let service: BookService;
  let bookRepository: Repository<BookEntity>;
  let memberRepository: Repository<MembersEntity>;
  let borrowedBookRepository: Repository<BorrowedBookEntity>;
  let suspendedMemberRepository: Repository<SuspendedMemberEntity>;
  let dataSource: DataSource;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BookService,
        {
          provide: getRepositoryToken(BookEntity),
          useClass: Repository,
        },
        {
          provide: getRepositoryToken(MembersEntity),
          useClass: Repository,
        },
        {
          provide: getRepositoryToken(BorrowedBookEntity),
          useClass: Repository,
        },
        {
          provide: getRepositoryToken(SuspendedMemberEntity),
          useClass: Repository,
        },
        {
          provide: DataSource,
          useValue: {
            createQueryRunner: jest.fn().mockReturnValue({
              connect: jest.fn(),
              startTransaction: jest.fn(),
              commitTransaction: jest.fn(),
              rollbackTransaction: jest.fn(),
              release: jest.fn(),
              manager: {
                query: jest.fn(),
              },
            }),
          },
        },
      ],
    }).compile();

    service = module.get<BookService>(BookService);
    bookRepository = module.get<Repository<BookEntity>>(getRepositoryToken(BookEntity));
    memberRepository = module.get<Repository<MembersEntity>>(getRepositoryToken(MembersEntity));
    borrowedBookRepository = module.get<Repository<BorrowedBookEntity>>(getRepositoryToken(BorrowedBookEntity));
    suspendedMemberRepository = module.get<Repository<SuspendedMemberEntity>>(getRepositoryToken(SuspendedMemberEntity));
    dataSource = module.get<DataSource>(DataSource);
  });

  describe('findBook', () => {
    it('should return a list of books', async () => {
      const expectedResult = [{ id: 1, code: 'B001', title: 'Book 1', author: 'Author 1', stock: 5 }];
      jest.spyOn(bookRepository, 'query').mockResolvedValue(expectedResult);

      const result = await service.findBook(1, 10, '', 'id', 'ASC');
      expect(result).toEqual(expectedResult);
    });
  });

  describe('borrowBook', () => {
    it('should borrow a book successfully', async () => {
      const mockQueryRunner = dataSource.createQueryRunner();
      jest.spyOn(bookRepository, 'query').mockResolvedValue([{ id: 1, stock: 5 }]);
      jest.spyOn(memberRepository, 'query').mockResolvedValue([{ id: 1 }]);
      jest.spyOn(borrowedBookRepository, 'query').mockResolvedValue([]);
      jest.spyOn(suspendedMemberRepository, 'query').mockResolvedValue([]);

      const result = await service.borrowBook('M001', 'B001');

      expect(mockQueryRunner.manager.query).toHaveBeenCalledWith(expect.stringContaining('INSERT INTO borrowed_book'), [1, 1]);
      expect(mockQueryRunner.commitTransaction).toHaveBeenCalled();
      expect(result).toBeNull();
    });

    it('should throw an error if the book is not found', async () => {
      jest.spyOn(bookRepository, 'query').mockResolvedValue([]);

      await expect(service.borrowBook('M001', 'B001')).rejects.toThrow(HttpException);
    });
  });

  describe('returnBook', () => {
    it('should return a book successfully', async () => {
      const mockQueryRunner = dataSource.createQueryRunner();
      jest.spyOn(bookRepository, 'query').mockResolvedValue([{ id: 1, stock: 5 }]);
      jest.spyOn(memberRepository, 'query').mockResolvedValue([{ id: 1 }]);
      jest.spyOn(borrowedBookRepository, 'query').mockResolvedValue([{ id: 1, created_at: new Date(Date.now() - 8 * 24 * 60 * 60 * 1000) }]);
      jest.spyOn(suspendedMemberRepository, 'query').mockResolvedValue([]);

      const result = await service.returnBook('M001', 'B001');

      expect(mockQueryRunner.manager.query).toHaveBeenCalledWith(expect.stringContaining('UPDATE borrowed_book'), [expect.any(Date), 1]);
      expect(mockQueryRunner.commitTransaction).toHaveBeenCalled();
      expect(result).toBeNull();
    });
  });

  describe('findMemberAndBookBorrowed', () => {
    it('should return a list of members with borrowed books count', async () => {
      const expectedResult = [
        {
          id: 3,
          code: "M003",
          name: "Putri",
          borrowed_books: "0"
        },
        {
          id: 2,
          code: "M002",
          name: "Ferry",
          borrowed_books: "0"
        },
        {
          id: 1,
          code: "M001",
          name: "Angga",
          borrowed_books: "1"
        }
      ];
      jest.spyOn(memberRepository, 'query').mockResolvedValue(expectedResult);

      const result = await service.findMemberAndBookBorrowed(1, 10, '', 'id', 'ASC');
      expect(result).toEqual(expectedResult);
    });
  });
});
