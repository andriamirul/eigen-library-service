import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BookModule } from './book/book.module';
import { DatabaseModule } from './database/database.module';
import { ConfigModule } from '@nestjs/config';
import { ReverseAlpbahetModule } from './algorithm-test/reverse-alphabet/reverse-alphabet.module';
import { LongestWordModule } from './algorithm-test/longest-word/longest-word.module';
import { CountWordModule } from './algorithm-test/count-word/longest-word.module';
import { DiagonalDifferenceModule } from './algorithm-test/diagonal-difference/diagonal-difference.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true,
    }),
    DatabaseModule,
    BookModule,
    ReverseAlpbahetModule,
    LongestWordModule,
    CountWordModule,
    DiagonalDifferenceModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
