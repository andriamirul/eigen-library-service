import {
    CreateDateColumn,
    DeleteDateColumn,
    UpdateDateColumn,
} from 'typeorm';

export class BaseEntity {
    @CreateDateColumn({ name: 'created_at' })
    created_at: any;

    @UpdateDateColumn({ name: 'updated_at' })
    updated_at: any;

    @DeleteDateColumn({ name: 'deleted_at' })
    deleted_at: any;
}
