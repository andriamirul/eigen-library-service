import { BaseEntity } from 'src/entities/base-entity';
import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    JoinColumn,
    OneToOne,
    OneToMany,
    ManyToOne,
} from 'typeorm';
import { MembersEntity } from './member.entity';

@Entity({ name: 'book' })
export class SuspendedMemberEntity extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int2' })
    id: number;

    @Column({ type: 'int2', nullable: false })
    member_id: number;

    @Column({ type: 'timestamptz', length: 255, nullable: true })
    suspended_until: any;

    @OneToOne(() => MembersEntity, member => member.suspendedMember)
    @JoinColumn({ name: 'member_id' })
    member: MembersEntity;
}
