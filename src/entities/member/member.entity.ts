import { BaseEntity } from 'src/entities/base-entity';
import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    JoinColumn,
    OneToOne,
    OneToMany,
} from 'typeorm';
import { BorrowedBookEntity } from '../book/borrowed-book.entity';
import { SuspendedMemberEntity } from './suspended-member.entity';

@Entity({ name: 'members' })
export class MembersEntity extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int2' })
    id: number;

    @Column({ type: 'varchar', length: 10, nullable: false })
    code: string;

    @Column({ type: 'varchar', length: 255, nullable: false })
    name: string;

    @OneToMany(() => BorrowedBookEntity, borrowedBook => borrowedBook.member)
    borrowedBooks: BorrowedBookEntity[];

    @OneToOne(() => SuspendedMemberEntity, suspendedMember => suspendedMember.member)
    suspendedMember: SuspendedMemberEntity;
}
