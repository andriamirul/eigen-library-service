import { BaseEntity } from 'src/entities/base-entity';
import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    JoinColumn,
    OneToOne,
    OneToMany,
    ManyToOne,
} from 'typeorm';
import { MembersEntity } from '../member/member.entity';
import { BookEntity } from './book.entity';

@Entity({ name: 'book' })
export class BorrowedBookEntity extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int2' })
    id: number;

    @Column({ type: 'int2', nullable: false })
    member_id: number;

    @Column({ type: 'int2', nullable: false })
    book_id: string;

    @Column({ type: 'timestamptz', length: 255, nullable: true })
    returned_at: any;

    @ManyToOne(() => MembersEntity, member => member.borrowedBooks)
    @JoinColumn({ name: 'member_id' })
    member: MembersEntity;

    @ManyToOne(() => BookEntity, book => book.borrowedBooks)
    @JoinColumn({ name: 'book_id' })
    book: BookEntity;

}
