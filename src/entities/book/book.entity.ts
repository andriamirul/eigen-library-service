import { BaseEntity } from 'src/entities/base-entity';
import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    JoinColumn,
    OneToOne,
    OneToMany,
} from 'typeorm';
import { BorrowedBookEntity } from './borrowed-book.entity';

@Entity({ name: 'book' })
export class BookEntity extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int2' })
    id: number;

    @Column({ type: 'varchar', length: 10, nullable: false })
    code: string;

    @Column({ type: 'varchar', length: 255, nullable: false })
    title: string;

    @Column({ type: 'varchar', length: 255, nullable: false })
    author: string;

    @Column({ type: 'int2', nullable: false })
    stock: number;

    @OneToMany(() => BorrowedBookEntity, borrowedBook => borrowedBook.book)
    borrowedBooks: BorrowedBookEntity[];
}
