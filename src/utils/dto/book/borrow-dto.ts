import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
    IsArray,
    IsBoolean,
    IsBooleanString,
    IsInt,
    IsNotEmpty,
    IsNumberString,
    IsOptional,
    IsString,
    Length,
} from 'class-validator';

export class BorrowDto {
    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    memberCode: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    bookCode: string;
}
