import { Controller, Get, Injectable, Param } from '@nestjs/common';
import { ReverseAlphabetService } from './reverse-alphabet.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Reverse Alphabet')
@Controller('reverse-alphabet')
@Injectable()
export class ReverseAlphabetController {
    constructor(private readonly reverseAlphabetService: ReverseAlphabetService) { }

    @Get('/reverse-alphabet/:input')
    reverseAlphabet(@Param('input') input: string): string {
        return this.reverseAlphabetService.reverseAlphabet(input);
    }
}
