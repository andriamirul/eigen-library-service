import { Module } from '@nestjs/common';
import { ReverseAlphabetController } from './reverse-alphabet.controller';
import { ReverseAlphabetService } from './reverse-alphabet.service';

@Module({
    imports: [
    ],
    controllers: [ReverseAlphabetController],
    providers: [ReverseAlphabetService]
})
export class ReverseAlpbahetModule { }
