import { Injectable } from '@nestjs/common';

@Injectable()
export class ReverseAlphabetService {
    reverseAlphabet(input: string): string {
        const letters = input.match(/[a-zA-Z]/g);
        const nonLetters = input.replace(/[a-zA-Z]/g, '');

        if (!letters) return nonLetters;

        const reversedLetters = letters.reverse().join('');
        return reversedLetters + nonLetters;
    }
}
