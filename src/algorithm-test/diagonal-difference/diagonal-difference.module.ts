import { Module } from '@nestjs/common';
import { DiagonalDifferenceController } from './diagonal-difference.controller';
import { DiagonalDifferenceService } from './diagonal-difference.service';

@Module({
    imports: [
    ],
    controllers: [DiagonalDifferenceController],
    providers: [DiagonalDifferenceService]
})
export class DiagonalDifferenceModule { }
