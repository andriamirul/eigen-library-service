import { Body, Controller, Injectable, Post } from "@nestjs/common";
import { ApiBody, ApiTags } from "@nestjs/swagger";
import { DiagonalDifferenceService } from "./diagonal-difference.service";

@ApiTags('Calculate Diagonal Difference')
@Controller('diagonal-difference')
@Injectable()
export class DiagonalDifferenceController {
    constructor(private readonly diagonalDifferenceService: DiagonalDifferenceService) { }

    @Post('/diagonal-difference')
    @ApiBody({
        schema: {
            type: 'array',
            items: {
                type: 'array',
                items: { type: 'number' }
            },
            minItems: 1,
            maxItems: 100
        }
    })
    calculateDiagonalDifference(@Body() matrix: number[][]): number {
        return this.diagonalDifferenceService.calculateDiagonalDifference(matrix);
    }
}