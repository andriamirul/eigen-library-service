import { Injectable } from '@nestjs/common';

@Injectable()
export class DiagonalDifferenceService {
    calculateDiagonalDifference(matrix: number[][]): number {
        let primaryDiagonalSum = 0;
        let secondaryDiagonalSum = 0;
        const n = matrix.length;

        for (let i = 0; i < n; i++) {
            primaryDiagonalSum += matrix[i][i];
            secondaryDiagonalSum += matrix[i][n - i - 1];
        }

        return Math.abs(primaryDiagonalSum - secondaryDiagonalSum);
    }
}
