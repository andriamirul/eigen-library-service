import { Controller, Get, Injectable, Param } from '@nestjs/common';
import { LongestWordService, } from './longest-word.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Longest Word')
@Controller('longest-word')
@Injectable()
export class LongsetWordController {
    constructor(private readonly longestWordService: LongestWordService) { }

    @Get('/longest-word/:input')
    reverseAlphabet(@Param('input') input: string): string {
        return this.longestWordService.getLongestWord(input);
    }
}
