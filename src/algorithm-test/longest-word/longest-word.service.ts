import { Injectable } from '@nestjs/common';

@Injectable()
export class LongestWordService {
    getLongestWord(input: string): string {
        const words = input.split(' ');

        let longestWord = '';

        for (const word of words) {
            if (word.length > longestWord.length) {
                longestWord = word;
            }
        }

        return longestWord;
    }
}
