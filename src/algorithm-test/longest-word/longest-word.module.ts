import { Module } from '@nestjs/common';
import { LongsetWordController } from './longest-word.controller';
import { LongestWordService } from './longest-word.service';

@Module({
    imports: [
    ],
    controllers: [LongsetWordController],
    providers: [LongestWordService]
})
export class LongestWordModule { }
