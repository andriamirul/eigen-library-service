import { Body, Controller, Injectable, Post } from "@nestjs/common";
import { ApiBody, ApiTags } from "@nestjs/swagger";
import { CountWordService } from "./count-word.service";

@ApiTags('Count Word From 2 Array')
@Controller('count-word')
@Injectable()
export class CountwWordController {
    constructor(private readonly countWordService: CountWordService) { }

    @Post('/count-words')
    @ApiBody({
        schema: {
            type: 'object',
            properties: {
                input: {
                    type: 'array',
                    items: { type: 'string' }
                },
                query: {
                    type: 'array',
                    items: { type: 'string' }
                }
            },
            required: ['input', 'query']
        }
    })
    countWords(
        @Body() data: { input: string[], query: string[] }
    ): number[] {
        const { input, query } = data;
        return this.countWordService.countWords(input, query);
    }
}