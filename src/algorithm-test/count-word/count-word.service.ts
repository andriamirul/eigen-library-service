import { Injectable } from '@nestjs/common';

@Injectable()
export class CountWordService {
    countWords(input: string[], query: string[]): number[] {
        const wordCounts: Record<string, number> = {};

        input.forEach(word => {
            wordCounts[word] = (wordCounts[word] || 0) + 1;
        });

        const counts: number[] = [];

        query.forEach(q => {
            counts.push(wordCounts[q] || 0);
        });

        return counts;
    }
}
