import { Module } from '@nestjs/common';
import { CountwWordController } from './count-word.controller';
import { CountWordService } from './count-word.service';

@Module({
    imports: [
    ],
    controllers: [CountwWordController],
    providers: [CountWordService]
})
export class CountWordModule { }
